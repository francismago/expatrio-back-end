package com.alvo.expatrio.exam.component.customer

import com.alvo.expatrio.exam.component.customer.exception.CustomerNotFoundException
import com.alvo.expatrio.exam.component.customer.param.CreateCustomerParam
import com.alvo.expatrio.exam.component.customer.param.UpdateCustomerParam
import com.alvo.expatrio.exam.component.customer.repository.CustomerEntity
import com.alvo.expatrio.exam.component.customer.repository.CustomerRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.lang.Exception
import java.time.LocalDateTime

@Service
class CustomerGatewayImpl(
        val customerRepository: CustomerRepository
) : CustomerGateway {

    override fun createCustomer(param: CreateCustomerParam): CustomerEntity {
        return customerRepository.save(
                CustomerEntity(
                        id = null,
                        firstName = param.firstName,
                        middleName = param.middleName,
                        lastName = param.lastName,
                        gender = param.gender.type,
                        birthDate = param.birthDate,
                        contactNumber = param.contactNumber,
                        email = param.email,
                        address = param.address,
                        createdAt = LocalDateTime.now(),
                        modifiedAt = LocalDateTime.now()
                )
        )
    }

    override fun editCustomer(customerId: Int, param: UpdateCustomerParam): CustomerEntity {
        val customer = try {
            customerRepository.findById(customerId).get()
        } catch (e: Exception) {
            throw CustomerNotFoundException()
        }
        return customerRepository.save(
                CustomerEntity(
                        id = customer.id,
                        firstName = param.firstName,
                        middleName = param.middleName,
                        lastName = param.lastName,
                        gender = param.gender.type,
                        birthDate = param.birthDate,
                        contactNumber = param.contactNumber,
                        email = param.email,
                        address = param.address,
                        createdAt = customer.createdAt,
                        modifiedAt = LocalDateTime.now()
                )
        )
    }

    override fun deleteCustomer(customerId: Int) {
        try {
            customerRepository.findById(customerId).get()
        } catch (e: Exception) {
            throw CustomerNotFoundException()
        }
        customerRepository.deleteById(customerId)
    }

    override fun getAllCustomers(offset: Int, limit: Int): List<CustomerEntity> {
        val page: Pageable = PageRequest.of(offset, limit)
        return customerRepository.findAllByOrderByModifiedAtDesc(page)
    }

    override fun getCustomerByEmail(email: String): CustomerEntity {
        val customer = customerRepository.findByEmail(email)
        if (customer.isEmpty) {
            throw CustomerNotFoundException()
        } else {
            return customer.get()
        }
    }
}