package com.alvo.expatrio.exam.component.user.exception

class UserNotFoundException : Exception("User not found.")