package com.alvo.expatrio.exam.component.user.repository

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<UserEntity, Int> {

    fun findOneByUsername(username: String): Optional<UserEntity>
}