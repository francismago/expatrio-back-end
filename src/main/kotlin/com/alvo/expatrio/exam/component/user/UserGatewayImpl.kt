package com.alvo.expatrio.exam.component.user

import com.alvo.expatrio.exam.component.user.exception.UserNotFoundException
import com.alvo.expatrio.exam.component.user.repository.UserEntity
import com.alvo.expatrio.exam.component.user.repository.UserRepository
import org.springframework.stereotype.Service
import java.lang.Exception

@Service
class UserGatewayImpl(
        val userRepository: UserRepository
) : UserGateway {
    override fun findByUsername(username: String): UserEntity {
        return try {
            userRepository.findOneByUsername(username).get()
        } catch (e: Exception) {
            throw UserNotFoundException()
        }
    }
}