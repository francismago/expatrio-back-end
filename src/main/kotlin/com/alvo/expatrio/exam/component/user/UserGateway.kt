package com.alvo.expatrio.exam.component.user

import com.alvo.expatrio.exam.component.user.repository.UserEntity

interface UserGateway {
    fun findByUsername(username: String): UserEntity
}