package com.alvo.expatrio.exam.component.customer.exception

class CustomerNotFoundException : Exception("Customer not found.")