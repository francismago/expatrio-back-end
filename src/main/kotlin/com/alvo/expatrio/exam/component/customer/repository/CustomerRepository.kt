package com.alvo.expatrio.exam.component.customer.repository

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CustomerRepository : JpaRepository<CustomerEntity, Int> {

    fun findByEmail(email: String): Optional<CustomerEntity>

    fun findAllByOrderByModifiedAtDesc(page: Pageable): List<CustomerEntity>
}