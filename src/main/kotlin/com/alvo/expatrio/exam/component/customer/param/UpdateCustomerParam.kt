package com.alvo.expatrio.exam.component.customer.param

import com.alvo.expatrio.exam.component.customer.constant.Gender
import java.time.LocalDate

class UpdateCustomerParam(
        val firstName: String,
        val middleName: String,
        val lastName: String,
        val gender: Gender,
        val birthDate: LocalDate,
        val contactNumber: String,
        val email: String,
        val address: String
)