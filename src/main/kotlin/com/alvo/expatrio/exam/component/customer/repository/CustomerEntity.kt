package com.alvo.expatrio.exam.component.customer.repository

import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "CUSTOMERS")
class CustomerEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int?,
        val firstName: String,
        val middleName: String,
        val lastName: String,
        val gender: String,
        val birthDate: LocalDate,
        val contactNumber: String,
        val email: String,
        @Column(columnDefinition = "VARCHAR(1000)")
        val address: String,
        val createdAt: LocalDateTime,
        val modifiedAt: LocalDateTime
)