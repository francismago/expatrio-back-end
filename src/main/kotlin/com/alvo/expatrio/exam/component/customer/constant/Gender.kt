package com.alvo.expatrio.exam.component.customer.constant

enum class Gender(
        val type: String
) {
    MALE("M"),
    FEMALE("F");


    companion object {
        fun getType(type: String): Gender {
            return values().first { type == it.type }
        }
    }
}