package com.alvo.expatrio.exam.component.customer

import com.alvo.expatrio.exam.component.customer.param.CreateCustomerParam
import com.alvo.expatrio.exam.component.customer.param.UpdateCustomerParam
import com.alvo.expatrio.exam.component.customer.repository.CustomerEntity

interface CustomerGateway {

    fun createCustomer(param: CreateCustomerParam): CustomerEntity

    fun editCustomer(customerId: Int, param: UpdateCustomerParam) : CustomerEntity

    fun deleteCustomer(customerId: Int)

    fun getAllCustomers(offset: Int, limit: Int): List<CustomerEntity>

    fun getCustomerByEmail(email: String): CustomerEntity
}