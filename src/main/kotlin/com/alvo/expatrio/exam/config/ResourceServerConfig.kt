package com.alvo.expatrio.exam.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices

@Configuration
@EnableResourceServer
class ResourceServerConfig(
        val tokenServices: ResourceServerTokenServices
) : ResourceServerConfigurerAdapter() {

    @Value("\${security.jwt.resource-id}")
    private val resourceIds: String? = null

    override fun configure(resources: ResourceServerSecurityConfigurer?) {
        resources!!.resourceId(resourceIds).tokenServices(tokenServices)
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.headers()
                .frameOptions()
                .sameOrigin()
            .and().csrf().disable()
                .authorizeRequests()
                .antMatchers(
                        "/login"
                )
                .permitAll()
                .antMatchers(HttpMethod.GET, "/customers")
                .permitAll()
                .antMatchers(
                        "/customers",
                        "/customers/**")
                .authenticated()
    }

}