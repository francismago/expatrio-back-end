package com.alvo.expatrio.exam.config

import com.alvo.expatrio.exam.component.customer.exception.CustomerNotFoundException
import com.alvo.expatrio.exam.component.user.exception.UserNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class ResponseExceptionHandlerConfig : DefaultHandlerExceptionResolver() {

    override fun doResolveException(
            request: HttpServletRequest,
            response: HttpServletResponse,
            handler: Any?, ex: Exception
    ): ModelAndView? {

        if (ex is CustomerNotFoundException || ex is UserNotFoundException) {
            return handleCustomException(response, ex)
        }
        response.sendError(HttpStatus.SERVICE_UNAVAILABLE.value(), ex.message)
        return ModelAndView()
    }

    fun handleCustomException(response: HttpServletResponse, ex: Exception): ModelAndView {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.message)
        return ModelAndView()
    }
}