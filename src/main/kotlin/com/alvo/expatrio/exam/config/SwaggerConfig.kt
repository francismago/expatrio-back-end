package com.alvo.expatrio.exam.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ParameterBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.schema.ModelRef
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig {
    @Bean
    fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.alvo.expatrio.exam"))
            .paths(PathSelectors.any())
            .build()
            .globalOperationParameters(listOf(
                    ParameterBuilder()
                            .name("Authorization")
                            .description("Authorization")
                            .defaultValue("Bearer {accessToken}")
                            .modelRef(ModelRef("string"))
                            .parameterType("header")
                            .required(false)
                            .build()
            ))
}