package com.alvo.expatrio.exam.config.security

import com.alvo.expatrio.exam.component.user.UserGateway
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class UserService(
        val userGateway: UserGateway
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        return userGateway.findByUsername(username)
    }
}