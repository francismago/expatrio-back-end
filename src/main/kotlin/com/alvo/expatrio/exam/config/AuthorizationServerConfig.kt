package com.alvo.expatrio.exam.config

import com.alvo.expatrio.exam.config.security.CustomTokenEnhancer
import com.alvo.expatrio.exam.config.security.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.CompositeTokenGranter
import org.springframework.security.oauth2.provider.TokenGranter
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter

@Configuration
@EnableAuthorizationServer
class AuthorizationServerConfig : AuthorizationServerConfigurerAdapter() {

    @Value("\${security.jwt.client-id}")
    private val appClientId: String? = null

    @Value("\${security.jwt.client-secret.encrypted}")
    private val appClientSecret: String? = null

    @Value("\${security.jwt.resource-id}")
    private val clientResourceId: String? = null

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var tokenStore: TokenStore

    @Autowired
    private lateinit var accessTokenConverter: JwtAccessTokenConverter

    @Autowired
    private lateinit var customTokenEnhancer: CustomTokenEnhancer

    @Throws(Exception::class)
    override fun configure(security: AuthorizationServerSecurityConfigurer) {
        security.tokenKeyAccess("permitAll()")
                .allowFormAuthenticationForClients()
                .checkTokenAccess("isAuthenticated()")
    }

    @Throws(Exception::class)
    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.inMemory()
                .withClient(appClientId)
                .secret(appClientSecret)
                .authorizedGrantTypes("password", "fingerprint", "pin")
                .scopes("resource-server-read", "resource-server-write")
                .resourceIds(clientResourceId)
                .accessTokenValiditySeconds(0)
    }

    @Throws(Exception::class)
    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        val tokenEnhancerChain = TokenEnhancerChain()
        tokenEnhancerChain.setTokenEnhancers(listOf(
                tokenEnhancer(),
                accessTokenConverter
        ))

        endpoints.tokenStore(tokenStore)
                .pathMapping("/oauth/token", "/login")
                .tokenEnhancer(tokenEnhancerChain)
                .authenticationManager(authenticationManager)
                .userDetailsService(userService)
                .tokenGranter(tokenGranter(endpoints))
                .exceptionTranslator { exception ->
                    when (exception) {
                        is OAuth2Exception -> {
                            return@exceptionTranslator ResponseEntity
                                    .status(HttpStatus.UNPROCESSABLE_ENTITY)
                                    .body(OAuth2Exception("Invalid Username or Password."))
                        }
                        is InternalAuthenticationServiceException -> return@exceptionTranslator ResponseEntity
                                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                                .body(OAuth2Exception(exception.message.toString()))
                        else -> throw exception
                    }
                }
    }

    @Bean
    fun tokenEnhancer(): TokenEnhancer {
        return customTokenEnhancer
    }

    fun tokenGranter(endpoints: AuthorizationServerEndpointsConfigurer): CompositeTokenGranter {
        return CompositeTokenGranter(listOf<TokenGranter>(
                endpoints.tokenGranter
        ))
    }
}