package com.alvo.expatrio.exam.config.security

import com.alvo.expatrio.exam.component.user.UserGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.stereotype.Component

@Component
class CustomTokenEnhancer @Autowired constructor(
        private val userGateway: UserGateway
) : TokenEnhancer {

    override fun enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken {
        val additionalInfo = mutableMapOf<String, Any>()
        val username = authentication.name
        additionalInfo["full_name"] = try {
            val user = userGateway.findByUsername(username)
            "${user.firstName} ${user.lastName}"
        } catch (ex: Exception) {
            ""
        }
        (accessToken as DefaultOAuth2AccessToken).additionalInformation = additionalInfo
        return accessToken
    }

}