package com.alvo.expatrio.exam.feature.customer.form

import java.time.LocalDate

class CustomerForm(
        val firstName: String?,
        val middleName: String?,
        val lastName: String?,
        val gender: String?,
        val birthDate: LocalDate?,
        val contactNumber: String?,
        val email: String?,
        val address: String?
)