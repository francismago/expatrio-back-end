package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.component.customer.CustomerGateway
import com.alvo.expatrio.exam.feature.customer.resp.CustomerResp
import org.springframework.stereotype.Service

@Service
class GetAllCustomersImpl(
        val customerGateway: CustomerGateway
) : GetAllCustomers {

    override fun execute(offset: Int, limit: Int): List<CustomerResp> {
        return customerGateway.getAllCustomers(offset.dec(), limit)
                .map { customer ->
                    CustomerResp(
                            id = customer.id!!,
                            firstName = customer.firstName,
                            middleName = customer.middleName,
                            lastName = customer.lastName,
                            gender = customer.gender,
                            birthDate = customer.birthDate,
                            contactNumber = customer.contactNumber,
                            email = customer.email,
                            address = customer.address,
                            createdAt = customer.createdAt,
                            modifiedAt = customer.modifiedAt
                    )

                }
    }
}