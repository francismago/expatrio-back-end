package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.component.customer.CustomerGateway
import com.alvo.expatrio.exam.component.customer.constant.Gender
import com.alvo.expatrio.exam.component.customer.param.CreateCustomerParam
import com.alvo.expatrio.exam.feature.customer.form.CustomerForm
import com.alvo.expatrio.exam.feature.customer.resp.CustomerResp
import org.springframework.stereotype.Service

@Service
class CreateCustomerImpl(
        val customerGateway: CustomerGateway
) : CreateCustomer {

    override fun execute(form: CustomerForm): CustomerResp {
        val customer = customerGateway.createCustomer(
                CreateCustomerParam(
                        firstName = form.firstName!!,
                        middleName = form.middleName!!,
                        lastName = form.lastName!!,
                        gender = Gender.getType(form.gender!!),
                        birthDate = form.birthDate!!,
                        contactNumber = form.contactNumber!!,
                        email = form.email!!,
                        address = form.address!!
                ))

        return CustomerResp(
                id = customer.id!!,
                firstName = customer.firstName,
                middleName = customer.middleName,
                lastName = customer.lastName,
                gender = customer.gender,
                email = customer.email,
                birthDate = customer.birthDate,
                contactNumber = customer.contactNumber,
                address = customer.address,
                createdAt = customer.createdAt,
                modifiedAt = customer.modifiedAt
        )
    }

}