package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.component.customer.CustomerGateway
import org.springframework.stereotype.Service

@Service
class DeleteCustomerImpl(
        val customerGateway: CustomerGateway
) : DeleteCustomer {

    override fun execute(customerId: Int) {
        customerGateway.deleteCustomer(customerId)
    }
}