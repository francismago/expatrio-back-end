package com.alvo.expatrio.exam.feature.customer.resp

import java.time.LocalDate
import java.time.LocalDateTime

class CustomerResp(
        val id: Int,
        val firstName: String,
        val middleName: String,
        val lastName: String,
        val gender: String,
        val birthDate: LocalDate,
        val contactNumber: String,
        val email: String,
        val address: String,
        val createdAt: LocalDateTime,
        val modifiedAt: LocalDateTime
)