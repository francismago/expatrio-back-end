package com.alvo.expatrio.exam.feature.customer

import com.alvo.expatrio.exam.feature.customer.form.CustomerForm
import com.alvo.expatrio.exam.feature.customer.interactor.CreateCustomer
import com.alvo.expatrio.exam.feature.customer.interactor.DeleteCustomer
import com.alvo.expatrio.exam.feature.customer.interactor.GetAllCustomers
import com.alvo.expatrio.exam.feature.customer.interactor.UpdateCustomer
import com.alvo.expatrio.exam.feature.customer.validator.CustomerFormValidator
import org.springframework.http.HttpStatus
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/customers")
class CustomerController(
        val createCustomer: CreateCustomer,
        val getAllCustomers: GetAllCustomers,
        val updateCustomer: UpdateCustomer,
        val deleteCustomer: DeleteCustomer
) {

    @InitBinder("customerForm")
    fun initEventFormBinder(binder: WebDataBinder) {
        binder.addValidators(CustomerFormValidator())
    }

    @GetMapping
    fun getAllCustomer(@RequestParam offset: Int, @RequestParam limit: Int) = getAllCustomers.execute(offset, limit)

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    fun createCustomer(@RequestBody @Valid customerForm: CustomerForm) = createCustomer.execute(customerForm)

    @PutMapping("/{id}")
    fun updateCustomer(@PathVariable id: Int, @RequestBody @Valid customerForm: CustomerForm) = updateCustomer.execute(id, customerForm)

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    fun deleteCustomer(@PathVariable id: Int) = deleteCustomer.execute(id)

}