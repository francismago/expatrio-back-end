package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.component.customer.CustomerGateway
import com.alvo.expatrio.exam.component.customer.constant.Gender
import com.alvo.expatrio.exam.component.customer.param.UpdateCustomerParam
import com.alvo.expatrio.exam.feature.customer.form.CustomerForm
import com.alvo.expatrio.exam.feature.customer.resp.CustomerResp
import org.springframework.stereotype.Service

@Service
class UpdateCustomerImpl(
        val customerGateway: CustomerGateway
) : UpdateCustomer {

    override fun execute(customerId: Int, form: CustomerForm): CustomerResp {
        val customer = customerGateway.editCustomer(
                customerId,
                UpdateCustomerParam(
                        firstName = form.firstName!!,
                        middleName = form.middleName!!,
                        lastName = form.lastName!!,
                        gender = Gender.getType(form.gender!!),
                        birthDate = form.birthDate!!,
                        contactNumber = form.contactNumber!!,
                        email = form.email!!,
                        address = form.address!!
                )
        )

        return CustomerResp(
                id = customer.id!!,
                firstName = customer.firstName,
                middleName = customer.middleName,
                lastName = customer.lastName,
                gender = customer.gender,
                email = customer.email,
                birthDate = customer.birthDate,
                contactNumber = customer.contactNumber,
                address = customer.address,
                createdAt = customer.createdAt,
                modifiedAt = customer.modifiedAt
        )
    }
}