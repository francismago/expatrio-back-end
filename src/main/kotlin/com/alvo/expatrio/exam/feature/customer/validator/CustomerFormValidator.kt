package com.alvo.expatrio.exam.feature.customer.validator

import com.alvo.expatrio.exam.feature.customer.form.CustomerForm
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator

class CustomerFormValidator : Validator {

    override fun validate(target: Any, errors: Errors) {
        ValidationUtils.rejectIfEmpty(errors, "firstName", "404", "First name required.")
        ValidationUtils.rejectIfEmpty(errors, "middleName", "404", "Middle name required.")
        ValidationUtils.rejectIfEmpty(errors, "lastName", "404", "Last name required.")
        ValidationUtils.rejectIfEmpty(errors, "gender", "404", "Gender required.")
        ValidationUtils.rejectIfEmpty(errors, "birthDate", "404", "Birth date required.")
        ValidationUtils.rejectIfEmpty(errors, "contactNumber", "404", "Mobile number required.")
        ValidationUtils.rejectIfEmpty(errors, "email", "404", "Email required.")
        ValidationUtils.rejectIfEmpty(errors, "address", "404", "Address required.")
    }

    override fun supports(clazz: Class<*>): Boolean {
        return CustomerForm::class.java.equals(clazz)
    }
}