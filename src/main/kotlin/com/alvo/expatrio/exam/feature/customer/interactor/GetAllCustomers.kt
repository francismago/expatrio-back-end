package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.feature.customer.resp.CustomerResp

interface GetAllCustomers {
    fun execute(offset: Int, limit: Int): List<CustomerResp>
}