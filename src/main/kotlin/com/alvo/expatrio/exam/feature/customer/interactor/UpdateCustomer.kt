package com.alvo.expatrio.exam.feature.customer.interactor

import com.alvo.expatrio.exam.feature.customer.form.CustomerForm
import com.alvo.expatrio.exam.feature.customer.resp.CustomerResp

interface UpdateCustomer {
    fun execute(customerId: Int, form: CustomerForm): CustomerResp
}