package com.alvo.expatrio.exam.feature.customer.interactor

interface DeleteCustomer {
    fun execute(customerId: Int)
}