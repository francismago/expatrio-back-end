package com.alvo.expatrio.exam.feature.customer

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local")
@AutoConfigureMockMvc
class CustomerControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser(username = "", password = "")
    fun `create account`() {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/customer")
                        .param("firstName","Francis")
                        .param("middleName","Rodeo")
                        .param("lastName","Mago")
                        .param("gender","M")
                        .param("birthDate","2002-02-02")
                        .param("contactNumder","01975037055")
                        .param("email","francis@mago.dev")
                        .param("address","88 naga st. PH")
                        .header("Authorization","Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYjY4NDQ1MGItNzNkMS00ODlhLWI2Y2YtMjMyOWUyNTFjZGY3Il0sImZ1bGxfbmFtZSI6ImZyYW5jaXMgbWFnbyIsInVzZXJfbmFtZSI6ImFkbWluQGdtYWlsLmNvbSIsInNjb3BlIjpbInJlc291cmNlLXNlcnZlci1yZWFkIiwicmVzb3VyY2Utc2VydmVyLXdyaXRlIl0sImF1dGhvcml0aWVzIjpbIkFETUlOIl0sImp0aSI6IjRiZTY5YTZlLThkYzctNGUzYi1hYWUyLWM1ZmQzNzg2NmE2ZSIsImNsaWVudF9pZCI6IjdiNGUxYWJhLWJhNWYtNGQzMi05MjcxLWM4MzE2YzJjMzhiOSJ9.2m55qYsuMy90p6IjqNljAVjaBuse94PAV6hZB6Z3eFU")

        ).andExpect(MockMvcResultMatchers.status().isCreated)
    }
}